# Generated by Cyber Sandbox Creator 3.1.0
# https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator
#
# -*- mode: ruby -*-
# vi: set ft=ruby :

ansible_groups = {
  "hosts" => ["server1", "attacker"], 
  "routers" => ["router"], 
  "ssh" => ["router", "server1", "attacker"], 
  "winrm" => [], 
  "ansible" => ["router", "server1", "attacker"], 
  "user-accessible" => ["attacker"]
}

Vagrant.configure("2") do |config|

  # Device(router): router
  config.vm.define "router" do |device|
    device.vm.hostname = "router"
    device.vm.box = "debian-10-x86_64"
    device.vm.provider "virtualbox" do |vb|
      vb.memory = 2048
      vb.cpus = 1
    end
    device.vm.synced_folder ".",
      "/vagrant",
      type: "rsync",
      rsync__exclude: ".git/"
    device.vm.network "private_network",
      virtualbox__intnet: "network",
      ip: "10.10.20.1",
      netmask: "255.255.255.0"
    device.vm.network "private_network",
      virtualbox__intnet: "internet-connection",
      ip: "100.100.100.1",
      netmask: "255.255.255.0"
    device.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "preconfig/playbook.yml"
      ansible.groups = ansible_groups
      ansible.limit = "router"
    end
    device.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "provisioning/playbook.yml"
      ansible.groups = ansible_groups
      ansible.extra_vars = "provisioning/extra_vars.yml"
      ansible.limit = "router"
    end
  end

  # Device(host): server1
  config.vm.define "server1" do |device|
    device.vm.hostname = "server1"
    device.vm.box = "debian-10-x86_64"
    device.vm.provider "virtualbox" do |vb|
      vb.memory = 1024
      vb.cpus = 1
    end
    device.vm.synced_folder ".",
      "/vagrant",
      type: "rsync",
      rsync__exclude: ".git/"
    device.vm.network "private_network",
      virtualbox__intnet: "network",
      ip: "10.10.20.2",
      netmask: "255.255.255.0"
    device.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "preconfig/playbook.yml"
      ansible.groups = ansible_groups
      ansible.limit = "server1"
    end
    device.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "provisioning/playbook.yml"
      ansible.groups = ansible_groups
      ansible.extra_vars = "provisioning/extra_vars.yml"
      ansible.limit = "server1"
    end
  end

  # Device(host): attacker
  config.vm.define "attacker" do |device|
    device.vm.hostname = "attacker"
    device.vm.box = "debian-10-x86_64"
    device.vm.provider "virtualbox" do |vb|
      vb.memory = 512
      vb.cpus = 1
    end
    device.vm.synced_folder ".",
      "/vagrant",
      type: "rsync",
      rsync__exclude: ".git/"
    device.vm.network "private_network",
      virtualbox__intnet: "network",
      ip: "10.10.20.4",
      netmask: "255.255.255.0"
    device.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "preconfig/playbook.yml"
      ansible.groups = ansible_groups
      ansible.limit = "attacker"
    end
    device.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "provisioning/playbook.yml"
      ansible.groups = ansible_groups
      ansible.extra_vars = "provisioning/extra_vars.yml"
      ansible.limit = "attacker"
    end
  end
end
